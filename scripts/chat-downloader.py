import json
from json2html import *
import sys
import requests


def get_newest_messages(p):
    cookies = {
        'remember_web_59ba36addc2b2f9401580f014c7f58ea4e30989d': sys.argv[2],
        'XSRF-TOKEN': sys.argv[3],
        'sketchers_united_session': sys.argv[4],
    }

    headers = {
        'Accept': 'application/json, text/plain, */*',
        'X-Socket-Id': '162774.1133213',
        'X-XSRF-TOKEN': sys.argv[3],
    }

    params = {
        'n': '50',
        'p': p
    }

    response = requests.get(f'https://sketchersunited.org/chats/{sys.argv[1]}/messages', params=params, cookies=cookies,
                        headers=headers)
    return response.json()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("fatal error with wrong usage: python3 chat_number remember_web_cookie xsrf_token sketchers_united_session")
    else:
        more_messages_to_get = True
        all_messages = []
        p = 0

        while more_messages_to_get:
            print(f"Getting {p}")
            newest_messages = get_newest_messages(p)
            if len(newest_messages) == 0 or newest_messages is None:
                more_messages_to_get = False
                break
            else:
                p = newest_messages[0]['id']
                all_messages.insert(0, newest_messages)

        with open("chat.json", "w") as file:
            json.dump(all_messages, file)

        with open("chat.json", "r") as file:
            json_input = json.load(file)

        output = json2html.convert(json=json_input)

        with open("chat.html", "w") as file:
            file.write(output)
