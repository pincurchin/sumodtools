const userId = USER_ID

async function fetchAllPostIds(url) {
    let postIds = [];
    while (url) {
        try {
            const response = await fetch(url, {
                "credentials": "include",
                "headers": {
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:128.0) Gecko/20100101 Firefox/128.0",
                    "Accept": "application/json, text/plain, */*",
                    "Accept-Language": "en-US,en;q=0.5",
                    "Sec-Fetch-Dest": "empty",
                    "Sec-Fetch-Mode": "cors",
                    "Sec-Fetch-Site": "same-origin"
                },
                "referrer": `https://sketchersunited.org/users/${userId}`,
                "method": "GET",
                "mode": "cors"
            });

            if (response.ok) {
                const data = await response.json();

                if (data.data && Array.isArray(data.data.posts) && data.data.posts.length > 0) {
                    postIds = postIds.concat(data.data.posts.map(post => post.id));
                } else {
                    console.log("No more posts found.");
                    url = null;
                    continue;
                }

                const next = data.next;

                if (next) {
                    url = `https://sketchersunited.org/users/${userId}/posts/?p=${next}`;
                } else {
                    url = null;
                }
            } else {
                console.error('Fetch error:', response.statusText);
                url = null;
            }
        } catch (error) {
            console.error('Request failed', error);
            url = null;
        }
    }

    return postIds;
}

async function deletePosts(postIds) {
    for (const postId of postIds) {
        await deletePost(postId);
    }
}

async function deletePost(postId) {
    const url = `https://sketchersunited.org/admin/posts/${postId}`;
    try {
        const response = await fetch(url, {
            "credentials": "include",
            "headers": {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:128.0) Gecko/20100101 Firefox/128.0",
                "Accept": "application/json, text/plain, */*",
                "Accept-Language": "en-US,en;q=0.5",
"X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').content,
            "X-XSRF-TOKEN": document.cookie.split("XSRF-TOKEN=")[1],
                "Sec-Fetch-Dest": "empty",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Site": "same-origin",
                "Priority": "u=0"
            },
            "referrer": `https://sketchersunited.org/admin/posts/${postId}`,
            "method": "DELETE",
            "mode": "cors"
        });

        if (response.ok) {
            console.log(`Post ${postId} deleted successfully.`);
        } else {
            console.error(`Failed to delete post ${postId}:`, response.statusText);
        }
    } catch (error) {
        console.error(`Error deleting post ${postId}:`, error);
    }
}

async function main() {
    const initialUrl = `https://sketchersunited.org/users/${userId}/posts`;
    const postIds = await fetchAllPostIds(initialUrl);

    if (postIds.length > 0) {
        console.log(`Found ${postIds.length} posts to delete.`);
        await deletePosts(postIds);
    } else {
        console.log("No posts found to delete.");
    }
}

main();