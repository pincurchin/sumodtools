// ==UserScript==
// @name        New script sketchersunited.org
// @namespace   Violentmonkey Scripts
// @match       https://sketchersunited.org/users/*
// @grant       none
// @version     1.0
// @author      -
// @description 26/09/2024, 11:23:35
// ==/UserScript==

'use strict';
const value = "Posts";
let editModeOn = false;
let deletionQueue = [];
const modMode = true;

const explorePage = "https://sketchersunited.org/posts"
const mePage = "https://sketchersunited.org/me"
const rateLimit = 500 //milliseconds

const items = Object.freeze({
    CHAT:   Symbol("chat"),
    COMMENT:  Symbol("comment"),
    MESSAGE: Symbol("message"),
    POST: Symbol("post"),
    REPORT: Symbol("report"),
});

function generateInfoBar() {
    let infoBar = document.createElement("nav")
    infoBar.className = "navbar position-relative py-sm-3 editMode"
    infoBar.style.backgroundColor = "#800000"

    let contentsDiv = document.createElement("div");
    contentsDiv.className = "d-flex align-items-center w-100 editMode";

    let editModeWarning = document.createElement("div");
    editModeWarning.innerHTML='<div class = "p-2 mx-2 editMode editModeTitle"><strong>⚠ DELETE MODE ON</strong></div>';

    let postsCountLabel = document.createElement("div");
    postsCountLabel.innerHTML='<div class = "p-2 mx-2 editMode">' + value + ' selected:</div>';

    let postsCount = document.createElement("div");
    postsCount.innerHTML='<div class = "p-2 mx-2 editMode itemsCount"><em>0</em></div>';

    let confirmButton = document.createElement("button");
    confirmButton.className = "btn btn-sm btn-primary editMode editModeButton"
    confirmButton.disabled = true;
    confirmButton.innerText = 'Delete';
    confirmButton.onclick = function() {sendDeleteRequests();};

    contentsDiv.appendChild(editModeWarning);
    contentsDiv.appendChild(postsCountLabel);
    contentsDiv.appendChild(postsCount);
    contentsDiv.appendChild(confirmButton);

    infoBar.appendChild(contentsDiv);

    let navDiv = document.querySelectorAll('div')[1]; //rewrite to xpath
    navDiv.appendChild(infoBar);
}

function openEditMode() {

    editModeOn = !editModeOn;

    if(editModeOn) {
        showCheckBoxes()
        disableLinks();
        generateInfoBar()
    }
    else {
        hideEditModeElements()
        enableLinks();
        deletionQueue = [];
    }
}

function getUserNumber() {
    let profileLink = document.querySelector('.name-profile').href;
    return profileLink.split("/")[4]
}

function getToken() {
    return document.getElementsByName("_token")[0].getAttribute("value")
}

function getMessages() {
    return document.querySelectorAll(".profile-message")
}

function generateCheckBox(value, style) {
    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.value = value;
    checkbox.className = "editMode";
    checkbox.setAttribute("style", style)
    checkbox.onchange = function() {setCheck(checkbox);};
    return checkbox;
}

function disableLinks() {
    document.querySelectorAll("a").forEach(function(node) {
        node.style.pointerEvents = "none"
    });

    document.querySelectorAll(".stretched-link").forEach(function(node) {
        node.style.pointerEvents = "none"
        node.style.display = "none"
    });
}

function enableLinks() {
    document.querySelectorAll("a").forEach(function(node) {
        node.style.pointerEvents = "auto"
    });

    document.querySelectorAll(".stretched-link").forEach(function(node) {
        node.style.pointerEvents = "auto"
        node.style.display = "initial"
    });
}

function hideEditModeElements() {
    document.querySelectorAll(".editMode").forEach(function(node) {
        node.remove();
    });
}

function createButton(navbar) {
    let editModeBar = document.createElement("li");
    editModeBar.style.marginRight = "1em";

    let editModeButton = document.createElement("button")
    editModeButton.type = "button";
    editModeButton.value = "editModeOn"
    editModeButton.className = "btn btn-sm btn-primary"
    editModeButton.innerHTML = "🗑 " + value
    editModeButton.onclick = function() {openEditMode();};

    editModeBar.appendChild(editModeButton);
    navbar.appendChild(editModeBar);
}

function updateDeletionIndicator() {
    let deleteButton = document.querySelector(".editModeButton");
    deleteButton.disabled = true;
    deleteButton.innerHTML = "<strong>DELETING...</strong>";
}

function generateDeletePostRequest(post) {
    let mainURL = "https://sketchersunited.org/posts/"

    if (modMode) {
        mainURL = "https://sketchersunited.org/admin/posts/"
    }

    let request = []
    request[0] = mainURL + parseInt(post)
    request[1] = {
        "credentials": "include",
        "headers": {
            "User-Agent": navigator.userAgent,
            "Accept": "application/json, text/plain, */*",
            "Accept-Language": navigator.language + ",en;q=0.5",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').content,
            "X-XSRF-TOKEN": document.cookie.split("XSRF-TOKEN=")[1],
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin"
        },
        "referrer": "https://sketchersunited.org/posts/" + parseInt(post),
        "method": "DELETE",
        "mode": "cors"
    }
    return request
}

function setCheck(checkbox) {
    if (checkbox.checked) {
        deletionQueue.push(checkbox.value);
    }
    else {
        deletionQueue = deletionQueue.filter(v => v !== checkbox.value);
    }

    let deleteButton = document.querySelector(".editModeButton");
    deleteButton.disabled = deletionQueue.length <= 0;

    let itemsCount = document.querySelector(".itemsCount");
    itemsCount.innerHTML = "<em>" + deletionQueue.length.toString() + "</em>"
    console.log(deletionQueue)
}

function showCheckBoxes() {
    document.querySelectorAll(".card-item").forEach(function(post) {
        let postNumber = post.innerHTML.split(">")[0];
        postNumber = postNumber.substring(16);
        postNumber = postNumber.split('"')[0];
        let checkbox = generateCheckBox(postNumber, "position:absolute;margin:1em;transform:scale(2.5);")
        post.appendChild(checkbox);
    });
}

async function sendDeleteRequests() {
    if (confirm("You will delete " + deletionQueue.length + " posts. This operation is irreversible.")) {
        updateDeletionIndicator()

        if (deletionQueue.length === 0) {
            console.log("Why bother?")
        } else {
            for (let post of deletionQueue) {
                let request = generateDeletePostRequest(post)
                await fetch(request[0], request[1])
            }
        }

        window.location.reload();
    }
}

(function init() {
    let navbar = document.querySelector('.navbar-nav');
    let userNumberPage = "https://sketchersunited.org/user/" + getUserNumber()

    if (navbar) {
        if (!modMode) {
            if (window.location.href !== userNumberPage &&
                window.location.href !== mePage) {
                throw "Not on own profile";
            }
        }

        if (window.location.href !== explorePage &&
            !window.location.href.includes("/users/") &&
            window.location.href !== mePage)
        {
            throw "Not on posts area";
        }

        createButton(navbar)
    }
    else {
        setTimeout(init, 0);
    }
})();