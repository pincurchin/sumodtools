lines = []

with open("questions.txt") as file_in:
    lines = file_in.read().replace("\n", "").split("~")

for line in lines:
    parts = line.split("=")
    question = parts[1][:-1]

    answer = parts[2][:-1]

    print("<tr>")
    print(f"    <td>{question}</td>")
    print(f"    <td>{answer}</td>")
    print("</tr>")
