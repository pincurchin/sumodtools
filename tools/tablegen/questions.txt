q=Will there be new badges added?
a=We mods don't have any information about if any new badges are to be added.
c=Badges,System
~
q=When will this app get updated?
a=The drawing tool will get updated in the future, but we mods don't have any information about future updates to the app.
c=System
~
q=What happens if you trace other's art?
a=If you trace somebody's art then you will get penalised as it is a copyright violation.
c=Copyright,Rules
~
q=Bring back the lobby/When will you bring back the lobby?
a=Unfortunately we can't bring back the lobby, it's humanly impossible to moderate it, and we can't leave it unmoderated, even with an automoderation robot it would not be possible to moderate it to a good enough standard.
c=System
~
q=What are the rules?
a=We have our rules here: https://sketchersunited.org/terms; Generally so long as you don't steal other people's art and act unpleasant you'll be fine.
c=Rules
~
q=What is this chat?
a=You can ask questions to the moderators of Sketchers United in here about the website/app/community works.
c=System
~
q=What happens after somebody is banned for being under 12?
a=They can appeal their ban when they turn 12 by emailing support@sketchersunited.org.
c=Rules
~
q=What is the maximum file size for backdrops/avatars?
a=2000kb.
c=System
~
q=How do you block someone?
a=Go to the person's profile that you want to block and click the three dots icon in the upper right corner to access the block option.
c=Action
~
q=How do you delete a conversation?
a=You can leave a conversation with a user on the mobile app, on the website you can't unless it is a group chat. If you want to delete your messages you will have to delete them one by one (leaving just makes the chat disappear from the list, the other user can still read the messages.)
c=Action
~
q=Can I trace my own art?
a=It's totally fine to trace your own stuff/photos you've taken, tracing is disallowed because it is a copyright violation against the original artist, but if you're tracing something that belongs to you, it isn't a copyright violation.
c=Copyright,Rules
~
q=Is profanity allowed?
a=Yes, you can say anything here so long as it isn't extremely sexual or containing slurs or hate speech.
c=Rules
~
q=How do you get your account suspended?
a=An account gets suspended when it breaks the rules with enough severity. The rules are here: https://sketchersunited.org/terms and there's more information about the suspension process at: https://sketchersunited.org/mod_guide.
c=Rules
~
q=How do you type pictures like some people do?/How do you make a text post?
a=If you mean a photo with text on it, that person has manually typed that text into the art app they're using, or if you mean a text post then they selected the text post option (web)/didn't add an photo to the post when they made their post (mobile.)
c=System,Posts
~
q=How do you add a profile picture?
a=On Android and iOS: Go to your profile and tap the three dots icon at the top right and click 'Edit profile'.
On Web: Tap the avatar in the top right and click 'Edit profile'.
c=System,Profile
~
q=How do you make a post?
a=Click the + button on the top right, it will open the post creation page.
c=System,Posts
~
q=How do you draw?
a=There is a drawing tool for Android which is in beta currently, you can sign up for it on the Google Play store; though, as it is in beta, it is instable and not recommended for serious drawing.
c=System,Posts
~
q=What can you do on this app?
a=Sketchers United is a small art community where you can post art you've drawn, you can like/comment/follow/chat/make collaborative art with others, but it doesn't have many of the advanced social features of other social medias like an algorithm or reposting others posts (which can be a plus or a negative depending on your opinion). There is a drawing tool available for Android, but it's currently in beta, so the vast majority of people here draw using other programs.
c=System
~
q=Why am I having trouble adding users to a chat?
a=There is a switch in the settings where somebody can only allow themselves to be messaged by users they follow, it's likely that the people you tried to add have that turned on and aren't following you.
c=System
~
q=How do I add someone to a group chat?
a=On Android:
On iOS: Tap the (i) button in the top right, tap "View participants" and then tap the + button in the top right.
On web: Click the "Add users" button on the right pane.
c=System,Chats,Action
~
q=How do you get followers?
a=To get followers, you can interact with the community, like others' posts, make nice comments, etc.
c=
~
q=How do you post something in one of the explore categories?/How do I post in a channel?
a=On Android and web: When you make a post, there is an option "Post this in", select the channel from the dropdown there.
On iOS: When you make a post, there is an option to  When you make a post, there is an option to set a channel, select the channel from the dropdown there.
c=Action,Post
~
q=Hi! How do you make people admin?
a=In a private chat, invite the user @subot then type /promote username admin (you can also type /help to see the other subot commands.)
c=System,Chats,Action
~
q=How can I set my profile to private/friends only?
a=unfortunately we don't have a "friends only" option, but you can somewhat have the same effect if you put all your posts on sensitive (which hides it from the Explore page and makes your posts only visible if somebody explicitly goes to your profile) and only interact with your friends. Though if you comment on a public comment section, anybody could view your profile, so you would have to be careful about that.
c=System
~
q=How do I use sensitive mode?/How do I use the sensitive filter?/How do I make a sensitive post?
a=When you make a post, there is a switch to set it to sensitive.
c=System,Posts
~
q=Why are my messages not sending in this chat?
a=The "Ask the mods" chat is rate limited to 1 message per minute to stop people from having offtopic conversations or from spamming the chat.
c=
~
q=Are we only allowed to post art or can it be anything?
a=Yes, you may post anything. The only limitation is not to break our rules.
c=
~
q=Can you add friends?
a=You can follow those who are your friends, but otherwise we don't have a specific friending function.
c=
~
q=How do you make a chat?
a=On Android and iOS: Go to the chats tab by pressing the chat balloon icon on the bottom bar, and the press the + button in the top right.
On web: Click the "Private" label on the sidebar and then click the "New chat group" button.
c=
~
q=Can you show your face
a=Yes, you can but if you're obviously a very young child, then the post will be removed to protect your privacy.
c=
~
q=Can you use an photo in a comment?
a=You can only do text comments here, if you want to show a photo to somebody you will have to message it to them.
15 hours ago
c=
~
q=Can we post videos?
a=No, Sketchers United doesn't support videos (and probably never will because they increase server costs exponentially.) If you want to share an animation, you can post it as a .gif
c=
~
q=How do I leave a group chat?
a=On Android: Tap the trashcan icon on the top right.
On iOS: Tap the (i) button on the top right, and then tap "Leave conversation".
On web: Either click the three dots icon on the top to view the sidebar, or with the sidebar already open, click the three dots icon next to your name in the member list and then "Leave this group".
c=
~
q=Add [feature]
a=If you have suggestions for Sketchers United, you can post them in the Suggestions channel so the developer can see them.
c=
~
q=What is a channel?
a=A channel allows you to categorise your posts and have them viewable to the public when they check out the channel you've posted in.
c=
~
q=Are there any bots here?
a=There is @subot, but it is mostly just for moderating chat groups. Otherwise there aren't any other bots.
c=
~
q=Can you change [x] feature?
a=We mods don't have control over the features of Sketchers United, but you can make a post in the Suggestions channel so the developer can see your suggestion.
c=
~
q=How can I disable notifications?
a=You can disable them in the settings of the app or in the Settings app on your phone.
c=
~
q=What is the age limit?
a=You must be 12 or above to use Sketchers United.
c=
~
q=How do I edit my profile?
a=On Android and iOS: Go to your profile by tapping the rightmost icon on the bottom bar and tap the three dots icon on the top right, then tap "Edit profile".
On web: Click on the avatar in the top right and then click "Edit profile".
c=
~
q=Are joint accounts allowed?/Can two people use the same account?
a=Yes.
c=
~
q=why is this the only public chat?
a=We used to have a public global chat, but it was removed for being an absolute mess that was impossible to moderate, so now this is the only public chat.
c=
~
q=Are anatomy studies allowed?
a=Artistic nudity is absolutely allowed so long as it is put on sensitive.
c=
~
q=Is suggestive art allowed?
a=Suggestive art is allowed so long as it is only borderline sexual and is put on sensitive.
c=
~
q=Why can't I follow anybody?
a=It is likely that you are following people too fast, you can only follow five people a minute, or you have reached the following limit of 1000.
c=
~
q=Why can't I send an photo in the chat?
a=It may be the photo is too large (there is a limit of 8mb as far as I know), the photo is corrupted or the wrong format, or there is simply an error with Sketchers United itself. Try again and if it still fails to send, then convert the photo to .jpg to fix corruption and retry.
c=
~
q=What is the maximum amount of people you can follow?
a=1000.
c=
~
q=How do I post a .gif?/How do I post an animation?
a=You can post a gif like a normal photo. Any animation you post can only be in the .gif format, there's no support for videos on Sketchers United.
c=
~
q=How do I post an animation?
a=Any animation you post can only be in the .gif format, there's no support for videos on Sketchers United.
c=
~
q=Can I use the sensitive content warning for [purpose]?
a=Yes, you use the sensitive content for anything you want, it just works to warn a user and/or hide the post from the global feed.
c=
~
q=Can I donate to Sketchers United?
a=Yes, there is a Patreon at https://www.patreon.com/sketchersunited.
c=
~
q=How do a join a chat?
a=The owner of the chat has to add you.
c=
~
q=Can people use a slur even if they claim it?
a=No, slurs are completely disallowed
c=
~
q=Is there a swearing filter?
a=There is no filter here.
c=
~
q=Will you give a warning on our post if it is against the rules in someway? Or if we've done something bad? Or would you just take it down or ban us?
a=Depending on the infringment we give a warning, but if it's a copyright violation or anything sexual then it goes to an immediate ban (length depends on the offense.)
c=
~
q=Why can you only post 5 things a day?
a=To prevent new accounts from spamming.
c=
q=will there ever be the possibility of another public chat other than this one?
a=No, at least not for the foreseeable future.
c=
~
q=How do you get polls?
a=You can post a poll when you're creating a post on https://sketchersunited.org.
c=
~
q=Why does it not let me follow over 100 people?
a=There's a follow limit of 100 per day (and 1000 overall.)
c=
~
q=Can I share other social medias?
a=Yes, you can share your other social media.
c=
~
q=What is copyright violation?
a=We disallow tracing/modifying other's art without their permission and reposting other's artworks without permission (whether or not it's got the author credited), unless it's for use in something like a collage, and you have a reasonable belief that the author of the artwork wouldn't disallow that use. We do have collabs here where you can draw on top of other user's art and repost that, so that kind of thing wouldn't count as a copyright violation.
c=
~
q=How do I an appeal a ban?
a=You can appeal a ban by emailing appeals@sketchersunited.org.
c=
~
q=Can moderators read messages/chats?
a=Only the developer can see them but they're only read in the case of illegal activity.
c=
~
q=Can we use bases?
a=Yes, so long as you credit them appropriately and include a link back to the original base.
c=